import Adafruit_BBIO.GPIO as GPIO
import time
import socket
#TCP setup
ECHO_SERVER_ADDRESS = "10.20.60.66"
ECHO_PORT = 7667
BRODCAST_PORT=4242
hostname = socket.gethostname()    
IPAddr = socket.gethostbyname(hostname)
host = ''
SERVER_INFO=''
def getServerIp():
	global SERVER_INFO
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)   #creation socket
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
	s.bind((host,BRODCAST_PORT))
	# s.listen()
	message, address = s.recvfrom(BRODCAST_PORT)
	SERVER_INFO=str(address)
	if SERVER_INFO!='':
		s.close()


def pushData (direction):	#fonction qui envoie les commandes sur le TCP socket
	global SERVER_IPADDR
	print(SERVER_IPADDR)
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)	#creation socket
	s.connect((SERVER_IPADDR, ECHO_PORT))		#connection
	s.setblocking(0)
	s.settimeout(0.01)
	s.sendall(direction.encode(encoding='ascii', errors='strict'))	#envoie
	s.close()

	#I/O setup
GPIO.setup("P8_11", GPIO.IN) #right
GPIO.setup("P8_12", GPIO.IN) #left
GPIO.setup("P8_13", GPIO.IN) #center
GPIO.setup("P8_14", GPIO.IN) #up
GPIO.setup("P8_15", GPIO.IN) #down

#disable direction
direction='K'

while(True):
	global SERVER_IPADDR
	getServerIp()
	time.sleep(1)
	print('waiting brodcast from server')
	if SERVER_INFO !='':
		iplen=0
		print('server brodcast reception')
		SERVER_INFO=SERVER_INFO.split(',')
		SERVER_IPADDR=str(SERVER_INFO[0])
		iplen=len(SERVER_IPADDR)
		SERVER_IPADDR=SERVER_IPADDR[2:iplen]
		iplen=len(SERVER_IPADDR)
		SERVER_IPADDR=SERVER_IPADDR[0:iplen-1]
		print(SERVER_IPADDR )
		time.sleep(1)
		break

while True:

	if GPIO.input("P8_11"):	#regarde si le bouton droit a ete appuye
		time.sleep(0.05)#debounce
		print("right")
		direction='R'
		pushData(direction)
		while GPIO.input("P8_11"):	#debounce
			pass
		time.sleep(0.05))#debounce

	if GPIO.input("P8_12"):	#regarde si le bouton gauche a ete appuye
		time.sleep(0.05))#debounce
		print("left")
		direction='L'
		pushData(direction)
		while GPIO.input("P8_12"):	#debounce
			pass
		time.sleep(0.05))#debounce

	if GPIO.input("P8_13"):	#regarde si le bouton centre a ete appuye
		time.sleep(0.05))#debounce
		print("center")
		direction='C'
		pushData(direction)
		while GPIO.input("P8_13"):	#debounce
			pass
		time.sleep(0.05))#debounce

	if GPIO.input("P8_14"):	#regarde si le bouton haut a ete appuye
		time.sleep(0.05))#debounce
		print("up")
		direction='U'
		pushData(direction)
		while GPIO.input("P8_14"):	#debounce
			pass
		time.sleep(0.05))#debounce

	if GPIO.input("P8_15"):	#regarde si le bouton bas a ete appuye
		time.sleep(0.05))#debounce
		print("down")
		direction='D'
		pushData(direction)
		while GPIO.input("P8_15"):	#debounce
			pass
		time.sleep(0.05))#debounce
