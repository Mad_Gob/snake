#-----------------------------------------------------------------------------------#
#Nicolas et Robin                                                                   #
#                                                                                   #
#                       programme serveur/Host du jeux snake                        #
#-----------------------------------------------------------------------------------#   
import socket
import time
import threading
import arcade
import random

#taille de la fenêtre
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 500
PLAY_WIDTH = SCREEN_WIDTH-40
PLAY_HEIGHT = SCREEN_HEIGHT-80

arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "SNAKE A SNAAAKE OOOOO ITS A SNAKE!!!")
arcade.set_background_color(arcade.color.BLUE)

class Point:
    def __init__(self, x=0, y=0):        
        self.x = x
        self.y = y


#-----------------------------------------------------------------------------------#
#Variables
global playerCount
global player1_present
global player2_present
global gameStarted          
global PLAYER1_IP
global PLAYER2_IP
global INPUT_IP
global PLAYER_IP_OPP
global dataDir_P1
global dataDir_P2
global dataCommand
global notConnected
global pointInit1_P1
global pointInit2_P1
global pointInit3_P1
global pointInit1_P2
global pointInit2_P2
global pointInit3_P2           
global snake_P1
global snake_P2

#initialisation de variables
dataDir_P1=""
dataDir_P2=""
dataCommand=""
X_P1=0
y_P1=0
X_P2=0
y_P2=0
i=8
notConnected=True
#Points de départ des serpent
pointInit1_P1=Point(120,80)
pointInit2_P1=Point(100,80)
pointInit3_P1=Point(80,80)
pointInit1_P2=Point(280,160)
pointInit2_P2=Point(260,160)
pointInit3_P2=Point(240,160)
food=Point(SCREEN_HEIGHT+60,SCREEN_HEIGHT+60)
snake_P1=[pointInit1_P1,pointInit2_P1,pointInit3_P1]
snake_P2=[pointInit1_P2,pointInit2_P2,pointInit3_P2]
sizeOfSnake_P1=3
sizeOfSnake_P2=3

DEATH_P1 = False
DEATH_P2 = False

#Le serveur récupère sont addresse IP
hostname = socket.gethostname()    
IPAddr = socket.gethostbyname(hostname)

print("Your Computer Name is:" + hostname)    
print("Your Computer IP Address is:" + IPAddr)   
HOST = IPAddr  
HOST2 = IPAddr  
PORT = 7667# Port pour le jeux
BRODCAST_PORT = 4242# Port pour le brodcast de détection
#adresse IP des joueur
PLAYER1_IP = ""
PLAYER2_IP = ""
INPUT_IP = ""
PLAYER_IP_OPP = ""
#-----------------------------------------------------------------------------------#



#Fonctions--------------------------------------------------------------------------#

#Génère un nouveau point pour la nourriture
def generateFood():
    global playerCount
    global gameStarted
    loopBraker=True
    newFood = Point(0,0)
    global food
    j=0
    #Do-while pour générer la nourriture
    while loopBraker==True:
        loopBraker=False
        j=0
        newFood.x=random.randint(20,PLAY_WIDTH+20)
        newFood.y=random.randint(20,PLAY_HEIGHT+20)
        #arrondi la position de la nourriture
        deltaX=newFood.x%20
        deltaY=newFood.y%20
        #vérifie si la nourriture est a l'extérieur de l'air de jeux
        if deltaX < 10:
            newFood.x=newFood.x-deltaX
        else:
            newFood.x=newFood.x+(20-deltaX)
        if deltaY < 10:
            newFood.y=newFood.y-deltaY
        else:
            newFood.y=newFood.y+(20-deltaY)
        if newFood.x >= PLAY_WIDTH+20:
            newFood.x = PLAY_WIDTH
        if newFood.y >= PLAY_HEIGHT+20:
            newFood.y = PLAY_HEIGHT
        #vérifie si la nourriture n'est pas sur le serpent du joueur 1    
        while j<sizeOfSnake_P1:         
            pointTest=Point(0,0)
            pointTest=snake_P1[j]
            j+=1
            if (newFood.x==pointTest.x) and (newFood.y==pointTest.y):
                #reste dans la boucle si il y a interférence
                loopBraker=True
        if (playerCount==2):
            j=0
            #vérifie si la nourriture n'est pas sur le serpent du joueur 2
            while j<sizeOfSnake_P2: 
                pointTest=snake_P2[j]
                j+=1
                if (newFood.x==pointTest.x) and (newFood.y==pointTest.y):    
                    #reste dans la boucle si il y a interférence
                    loopBraker=True
    #associe les coordonnée de la nouvelle nourriture                
    food=newFood
    print("food X", food.x, 'y',food.y)

#détection de colision du serpent avec lui même pour les deux joueurs
def snakeCollsion():
    k=1
    global DEATH_P1
    global DEATH_P2
    global playerCount
    #compare les coordonnées de la tête du serpent 1 avec le reste du corps
    while k<sizeOfSnake_P1:
        head = Point(0,0)
        head=snake_P1[0]
        pointTest=Point(0,0)
        pointTest=snake_P1[k]
        k+=1
        if (head.x==pointTest.x) and (head.y==pointTest.y):
            DEATH_P1 = True
            break
    if playerCount==2:
        k=1
        #compare les coordonnées de la tête du serpent 2 avec le reste du corps
        while k<sizeOfSnake_P2:
            head = Point(0,0)
            head=snake_P2[0]
            pointTest=Point(0,0)
            pointTest=snake_P2[k]
            k+=1
            if (head.x==pointTest.x) and (head.y==pointTest.y):
                DEATH_P2 = True    
                break
#brodcats sur le réseau pour que les manette puisse connaitre l'adresse IP du Serveur
def broadcast():
    global notConnected
    while notConnected==True:
        dest = ('<broadcast>',BRODCAST_PORT)
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        s.sendto(HOST2.encode('utf-8') , dest)
        print ("Remote discovery brodcast ")
        time.sleep(1)
    

#Fonction du thread réseau 
def reseau():#fonction de serveur
    global player1_present
    player1_present=False
    global player2_present
    player2_present=False
    global playerCount
    playerCount=0
    global dataCommand
    while True:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s: 
            s.bind((IPAddr, PORT))#creation du socket
            s.listen()#ecoute le socket
            conn, addr = s.accept()#attend une donnée entrante
            dataIn = (conn.recv(1024)).decode(encoding='ascii',errors='strict')

            #Extaraction de l'adresse IP du packet entrant
            INPUT_IP=str(addr)
            iplen=0;
            INPUT_IP=INPUT_IP.split(',')
            PLAYER_IP_OPP=str(INPUT_IP[0])
            iplen=len(PLAYER_IP_OPP)
            PLAYER_IP_OPP=PLAYER_IP_OPP[2:iplen]
            iplen=len(PLAYER_IP_OPP)
            PLAYER_IP_OPP=PLAYER_IP_OPP[0:iplen-1]
            #acquisition des adresse IP des joueur
            if(playerCount==0):
                PLAYER1_IP=PLAYER_IP_OPP
                if dataIn == 'U':
                    dataCommand = 'F'
                    player1_present=True
                    playerCount=1
            elif gameStarted==False:
                if(playerCount==1):
                    if(PLAYER1_IP!=PLAYER_IP_OPP):
                        PLAYER2_IP=PLAYER_IP_OPP
                        if dataIn == 'U':
                            dataCommand = 'F'
                            player2_present=True                
                            playerCount=2
            with conn:
                while True:
                    global dataDir_P1
                    global dataDir_P2
                    #Lecture de la commande entrante et associe celle-ci au bon joueur
                    if(PLAYER_IP_OPP==PLAYER1_IP):#Joueur 1
                        print("command from P1: "+PLAYER_IP_OPP)
                        if dataIn=='U' or dataIn=='D' or dataIn=='L' or dataIn=='R':
                            #Empêche de revenir sur soit même
                            if dataDir_P1=='U' and dataIn=='D':
                                pass
                            elif dataDir_P1=='D' and dataIn=='U':
                                pass
                            elif dataDir_P1=='L' and dataIn=='R':
                                pass
                            elif dataDir_P1=='R' and dataIn=='L':
                                pass
                            else:#association de la commande de direction
                                dataDir_P1=dataIn
                                dataCommand = 'F'
                        elif dataIn=='C':#association de la command spécial (démarer la partie) P1 only
                            dataCommand=dataIn
                        else:
                            dataIn = 'F'
                            dataCommand = 'F'
                        break
                    
                    if (playerCount==2):#Joueur 2
                        if(PLAYER_IP_OPP==PLAYER2_IP):
                            print("command from P2: "+PLAYER_IP_OPP)
                            if dataIn=='U' or dataIn=='D' or dataIn=='L' or dataIn=='R':
                                #Empêche de revenir sur soit même
                                if dataDir_P2=='U' and dataIn=='D':
                                    pass
                                elif dataDir_P2=='D' and dataIn=='U':
                                    pass
                                elif dataDir_P2=='L' and dataIn=='R':
                                    pass
                                elif dataDir_P2=='R' and dataIn=='L':
                                    pass
                                else:#association de la commande de direction
                                    dataDir_P2=dataIn
                            break
                    else:
                        break
					
#Fonction du thread de déroulement du jeux                
def game():
    global dataCommand
    global playerCount
    global gameStarted
    global notConnected
    global sizeOfSnake_P1
    global sizeOfSnake_P2
    global player1_present
    global y_P1        
    global x_P1        
    global y_P2        
    global x_P2
    global dataDir_P1
    global dataDir_P2
    global DEATH_P1
    global DEATH_P2

    while True:
        y_P1=80
        x_P1=120
        y_P2=160
        x_P2=280
        gameStarted=False
        print("Waiting for start command")
        #while dataCommand!='C':#attend la condition de départ du jeux (bouton centre)
        while True:
            print("Waiting for players IP  ")
            time.sleep(1)
            if player1_present == True:
                if dataCommand == 'C':
                    gameGo=True
                    time.sleep(3)#délai avan le début de la partie
                    break
        notConnected=False
        gameStarted=True
        
        dataDir_P1="R"#direction initiale du joueur 1
        dataDir_P2="R"#direction initiale du joueur 2
        generateFood()#génère la première nouriture
        
        while gameGo==True:#boucle du jeu
            #incrémentation des coordonnée de la tête au 0.5 seconde
            print('game running '+str(gameStarted))
            #incrémentation des coordonnée de la tête du joueur 1
            if dataDir_P1=='U':
                y_P1+=20
                if y_P1 >= PLAY_HEIGHT+20:
                    DEATH_P1=True
                    pass
            elif dataDir_P1=='D':
                y_P1-=20
                if y_P1 < 20:
                    DEATH_P1=True
                    pass
            elif dataDir_P1=='R':
                x_P1+=20
                if x_P1 >= PLAY_WIDTH+20:
                    DEATH_P1=True
                    pass
            elif dataDir_P1=='L':
                x_P1-=20
                if x_P1 <20:
                    DEATH_P1=True
                    pass
            p_P1=Point(x_P1,y_P1)
            snake_P1.insert(0,p_P1)
            if playerCount==2:
                #incrémentation des coordonnée de la tête du joueur 2            
                if dataDir_P2=='U':
                    y_P2+=20
                    if y_P2 >= PLAY_HEIGHT+20:
                        DEATH_P2=True
                        pass
                elif dataDir_P2=='D':
                    y_P2-=20
                    if y_P2 < 20:
                        DEATH_P2=True
                        pass
                elif dataDir_P2=='R':
                    x_P2+=20
                    if x_P2 >= PLAY_WIDTH+20:
                        DEATH_P2=True
                        pass
                elif dataDir_P2=='L':
                    x_P2-=20
                    if x_P2 <20:
                        DEATH_P2=True
                        pass
                p_P2=Point(x_P2,y_P2)
                snake_P2.insert(0,p_P2)
                
            #détection de la colision entre le serpent et la nourriture
            head = snake_P1[0]
            if head.x == food.x:
                if head.y == food.y:
                    sizeOfSnake_P1=sizeOfSnake_P1+1
                    generateFood()
                else:
                    snake_P1.pop(sizeOfSnake_P1)
            else:
               snake_P1.pop(sizeOfSnake_P1)
            if playerCount==2:
                head_P2 = snake_P2[0]
                if head_P2.x == food.x:
                    if head_P2.y == food.y:
                        sizeOfSnake_P2=sizeOfSnake_P2+1
                        generateFood()
                    else:
                        snake_P2.pop(sizeOfSnake_P2)
                else:
                    snake_P2.pop(sizeOfSnake_P2)
            snakeCollsion()#vérifie les colision entre les serpent et eux même
            #détection de la colision entre les deux serpents
            if playerCount==2:
                k=1
                #vérifie la colision du serpent 1 sur le serpent 2
                while k<sizeOfSnake_P2:
                    head = Point(0,0)
                    head=snake_P1[0]
                    pointTest=Point(0,0)
                    pointTest=snake_P2[k]
                    k+=1
                    if (head.x==pointTest.x) and (head.y==pointTest.y):
                        DEATH_P1=True
                k=1
                #vérifie la colision du serpent 2 sur le serpent 1
                while k<sizeOfSnake_P1:
                    head = Point(0,0)
                    head=snake_P2[0]
                    pointTest=Point(0,0)
                    pointTest=snake_P1[k]
                    k+=1
                    if (head.x==pointTest.x) and (head.y==pointTest.y):
                        DEATH_P2=True
                #vérifie la colision des deux tête des serpent
                head_P1 = Point(0,0)
                head_P1=snake_P1[0]
                head_P2 = Point(0,0)
                head_P2=snake_P2[0]
                if (head_P1.x==head_P2.x) and (head_P1.y==head_P2.y):
                    DEATH_P1=True
                    DEATH_P2=True
            time.sleep(0.25)#délais entre les mouvement du serpent
            if(DEATH_P1==True)or(DEATH_P2==True):
                #empêge le jeux de continuer en réinitilisant des variables
                gameGo=False
                player1_present=False
                player2_present=False
                dataCommand='F'
                gameStarted=False
                time.sleep(2)#délai avant le retour au début de la fonction jeux
            
		
#Dessine chaque carré du serpent aux coordonnée x et y
def drawPoint(pX,pY,playerNumber):
    if(playerNumber==1):
        arcade.draw_xywh_rectangle_filled(pX, pY, 20, 20,arcade.color.WHITE)
        arcade.draw_xywh_rectangle_filled(pX+2, pY+2, 16, 16,arcade.color.GREEN)
    elif(playerNumber==2):
        arcade.draw_xywh_rectangle_filled(pX, pY, 20, 20,arcade.color.WHITE)
        arcade.draw_xywh_rectangle_filled(pX+2, pY+2, 16, 16,arcade.color.BLUE)    
#dessine la nourriture
def drawFood(pX,pY):
    arcade.draw_xywh_rectangle_filled(pX+2, pY+2, 16, 16,arcade.color.RED)
#dessine les bordures du jeux
def drawBorder():    
    arcade.draw_xywh_rectangle_filled(0, SCREEN_HEIGHT-40, SCREEN_WIDTH, 60,arcade.color.LAVENDER_MIST  )#score board    
    arcade.draw_xywh_rectangle_filled(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-40,arcade.color.JAZZBERRY_JAM )#play area
    arcade.draw_xywh_rectangle_filled(20, 20,PLAY_WIDTH,PLAY_HEIGHT,arcade.color.WHITE)

#demarrage des trois threads    
t = threading.Thread(target=reseau)
tt = threading.Thread(target=game)
ttt = threading.Thread(target=broadcast)
t.start()
tt.start()
ttt.start()
    
    
#Boucle d'affichage du jeux
while True:
    score_P1=0
    score_P2=0
    global playerCount
    global player1_present
    global player2_present
    global gameStarted
    
    #start menu---------------------------------------------------------------------------#
    while gameStarted==False:
        arcade.start_render()
        drawBorder()#dessine les bordures du jeux
        #Indicateur de présence du joueur 1
        if(player1_present==True):
            arcade.draw_circle_filled((SCREEN_WIDTH/2)-100,(SCREEN_HEIGHT/2), 30, arcade.color.LAPIS_LAZULI )
        else:
            arcade.draw_circle_outline((SCREEN_WIDTH/2)-100,(SCREEN_HEIGHT/2), 30, arcade.color.LAPIS_LAZULI )
        #Indicateur de présence du joueur 2
        if(player2_present==True):
            arcade.draw_circle_filled((SCREEN_WIDTH/2)+100,(SCREEN_HEIGHT/2), 30, arcade.color.RED )
        else:
            arcade.draw_circle_outline((SCREEN_WIDTH/2)+100,(SCREEN_HEIGHT/2), 30, arcade.color.RED )            
        
        arcade.draw_text("P1",
                         (SCREEN_WIDTH/2)-100, SCREEN_HEIGHT/2, arcade.color.BLACK, 30, width=SCREEN_WIDTH, align="center",
                         anchor_x="center", anchor_y="center")
        arcade.draw_text("P2",
                         (SCREEN_WIDTH/2)+100, SCREEN_HEIGHT/2, arcade.color.BLACK, 30, width=SCREEN_WIDTH, align="center",
                         anchor_x="center", anchor_y="center")

        #instructions
        arcade.draw_text("Appuyez sur UP pour joindre la partie",
                SCREEN_WIDTH/2, SCREEN_HEIGHT/2+80, arcade.color.BLACK, 20, width=SCREEN_WIDTH, align="center",
                anchor_x="center", anchor_y="center")
        if(player1_present==True):
            arcade.draw_text("Appuyez sur CENTER pour commencer",
                    SCREEN_WIDTH/2, SCREEN_HEIGHT/2-80, arcade.color.BLACK, 20, width=SCREEN_WIDTH, align="center",
                    anchor_x="center", anchor_y="center")
        arcade.finish_render()        
    #################################################################################################

    #Boucle d'affichage du jeux en marche-----------------------------------------------------------#
    arcade.start_render()
    nbCarre=0
    foodToDraw = Point(0,0)
    foodToDraw=food
    drawBorder()
    #dessine les serpents
    while(nbCarre < sizeOfSnake_P1):
        dotToWrite = snake_P1[nbCarre]
        if(nbCarre==0):
            arcade.draw_circle_filled(dotToWrite.x+10, dotToWrite.y+10, 8, arcade.color.LAPIS_LAZULI )    
        else:
            drawPoint(dotToWrite.x,dotToWrite.y,1)        
        nbCarre+=1
    if playerCount==2:
        nbCarre=0
        while(nbCarre < sizeOfSnake_P2):
            dotToWrite = snake_P2[nbCarre]
            if(nbCarre==0):
                arcade.draw_circle_filled(dotToWrite.x+10, dotToWrite.y+10, 8, arcade.color.GREEN )    
            else:
                drawPoint(dotToWrite.x,dotToWrite.y,2)
            nbCarre+=1
        
    drawFood(foodToDraw.x,foodToDraw.y)#dessine la nourriture
    #dessine le score
    if (playerCount==1):
        score_P1=(sizeOfSnake_P1-3)*10    
        arcade.draw_text("SCORE: "+str(score_P1),
                         SCREEN_WIDTH/2, SCREEN_HEIGHT-20, arcade.color.BLACK, 20, width=SCREEN_WIDTH, align="center",
                         anchor_x="center", anchor_y="center")
    else:
        score_P1=(sizeOfSnake_P1-3)*10
        arcade.draw_text("SCORE PLAYER 1: "+str(score_P1),
                         SCREEN_WIDTH/2-250, SCREEN_HEIGHT-10, arcade.color.BLACK, 20, width=SCREEN_WIDTH, anchor_x="left", anchor_y="top")
        score_P2=(sizeOfSnake_P2-3)*10
        arcade.draw_text("SCORE PLAYER 2: "+str(score_P2),
                         SCREEN_WIDTH/2+50, SCREEN_HEIGHT-10, arcade.color.BLACK, 20, width=SCREEN_WIDTH, anchor_x="left", anchor_y="top")   
    arcade.finish_render()
    #--------------------------------------------------------------------------------------------------#

    #Boucle lorsque un serpent est mort----------------------------------------------------------------#
    while(DEATH_P1==True)or(DEATH_P2==True):
        print('GAME OVER')
        arcade.start_render()
        arcade.set_background_color(arcade.color.RED)
        #résolution du jeux a deux joueur
        if (playerCount==2):
            #si les deux joueur meurt, le gagnant est celui dont le score est le plus élevé
            if(DEATH_P1==True)and(DEATH_P2==True):
                if score_P1>score_P2:
                    arcade.draw_text("PLAYER 1 WIN",
                                SCREEN_WIDTH/2, SCREEN_HEIGHT/2+60, arcade.color.BLACK, 30, width=SCREEN_WIDTH, align="center",
                                anchor_x="center", anchor_y="center")
                    arcade.draw_text("PLAYER 1 SCORE: "+str(score_P1),
                                SCREEN_WIDTH/2, (SCREEN_HEIGHT/2)-25, arcade.color.BLACK, 20, width=SCREEN_WIDTH, align="center",
                                anchor_x="center", anchor_y="center")
                elif score_P2>score_P1:
                    arcade.draw_text("PLAYER 2 WIN",
                                SCREEN_WIDTH/2, SCREEN_HEIGHT/2+60, arcade.color.BLACK, 30, width=SCREEN_WIDTH, align="center",
                                anchor_x="center", anchor_y="center")
                    arcade.draw_text("PLAYER 2 SCORE: "+str(score_P2),
                                SCREEN_WIDTH/2, (SCREEN_HEIGHT/2)-25, arcade.color.BLACK, 20, width=SCREEN_WIDTH, align="center",
                                anchor_x="center", anchor_y="center")
                else:
                    arcade.draw_text("DRAW",
                                SCREEN_WIDTH/2, SCREEN_HEIGHT/2+60, arcade.color.BLACK, 30, width=SCREEN_WIDTH, align="center",
                                anchor_x="center", anchor_y="center")
                    arcade.draw_text("BOTH SCORES: "+str(score_P1),
                                SCREEN_WIDTH/2, (SCREEN_HEIGHT/2)-25, arcade.color.BLACK, 20, width=SCREEN_WIDTH, align="center",
                                anchor_x="center", anchor_y="center")
            #si le joueur 2 meurt le joueur 1 gagne        
            elif (DEATH_P2==True):
                arcade.draw_text("PLAYER 1 WIN",
                             SCREEN_WIDTH/2, SCREEN_HEIGHT/2+60, arcade.color.BLACK, 30, width=SCREEN_WIDTH, align="center",
                             anchor_x="center", anchor_y="center")
                arcade.draw_text("SCORE: "+str(score_P1),
                         SCREEN_WIDTH/2, (SCREEN_HEIGHT/2)-25, arcade.color.BLACK, 20, width=SCREEN_WIDTH, align="center",
                         anchor_x="center", anchor_y="center")
            #si le joueur 1 meurt le joueur 2 gagne    
            else:
                arcade.draw_text("PLAYER 2 WIN",
                             SCREEN_WIDTH/2, SCREEN_HEIGHT/2+60, arcade.color.BLACK, 30, width=SCREEN_WIDTH, align="center",
                             anchor_x="center", anchor_y="center")
                arcade.draw_text("SCORE: "+str(score_P2),
                         SCREEN_WIDTH/2, (SCREEN_HEIGHT/2)-25, arcade.color.BLACK, 20, width=SCREEN_WIDTH, align="center",
                         anchor_x="center", anchor_y="center")
        #résolution du jeux a un joueur, affiche simplement le score
        else:
            arcade.draw_text("SCORE: "+str(score_P1),
                         SCREEN_WIDTH/2, (SCREEN_HEIGHT/2)-25, arcade.color.BLACK, 20, width=SCREEN_WIDTH, align="center",
                         anchor_x="center", anchor_y="center")

        arcade.draw_text("GAME OVER",
                         SCREEN_WIDTH/2, SCREEN_HEIGHT/2+25, arcade.color.BLACK, 40, width=SCREEN_WIDTH, align="center",
                         anchor_x="center", anchor_y="center")
        #instruction pour recommencer
        arcade.draw_text("Appuyez sur CENTER pour retourner au menu",
                    SCREEN_WIDTH/2, SCREEN_HEIGHT/2-120, arcade.color.BLACK, 20, width=SCREEN_WIDTH, align="center",
                    anchor_x="center", anchor_y="center")
        #Condition de réinitialisation du jeux lorsque la touche du centre est appuyé
        if dataCommand=='C':   
            #réinitialisation de toute les variables
            score_P1=0
            score_P2=0
            gameStarted=False
            player1_present=False
            player2_present=False
            DEATH_P1 = False
            DEATH_P2 = False
            sizeOfSnake_P1=3
            sizeOfSnake_P2=3
            playerCount=0
            PLAYER1_IP = ""
            PLAYER2_IP = ""
            INPUT_IP = ""
            PLAYER_IP_OPP = ""
            dataDir_P1="R"#direction initiale
            dataDir_P2="R"#direction initiale
            notConnected=True
            pointInit1_P1=Point(120,80)
            pointInit2_P1=Point(100,80)
            pointInit3_P1=Point(80,80)
            pointInit1_P2=Point(280,160)
            pointInit2_P2=Point(260,160)
            pointInit3_P2=Point(240,160)            
            snake_P1=[pointInit1_P1,pointInit2_P1,pointInit3_P1]
            snake_P2=[pointInit1_P2,pointInit2_P2,pointInit3_P2]
            time.sleep(4)#délais avant le retour au menu 
            dataCommand='F'            
            break            
        arcade.finish_render()